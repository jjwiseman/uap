//! Defines an error type.

use thiserror::Error;

/// The error type.
#[derive(Debug, Error, PartialOrd, PartialEq, Ord, Eq)]
#[non_exhaustive]
pub enum Error {
    /// Error related to loading a database.
    #[error("{0}")]
    DatabaseLoadError(String),
}
